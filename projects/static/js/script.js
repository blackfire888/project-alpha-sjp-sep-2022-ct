
// Selecting elements
const player0El = document.querySelector('.player--0');
const player1El = document.querySelector('.player--1');
const player2El = document.querySelector('.player--2');
const player3El = document.querySelector('.player--3');
const score0El = document.querySelector('#score--0');
const score1El = document.getElementById('score--1');
const score2El = document.querySelector('#score--2');
const score3El = document.getElementById('score--3');
const current0El = document.getElementById('current--0');
const current1El = document.getElementById('current--1');
const current2El = document.getElementById('current--2');
const current3El = document.getElementById('current--3');

const diceEl = document.querySelector('.dice');
const btnNew = document.querySelector('.btn--new');
const btnRoll = document.querySelector('.btn--roll');
const btnHold = document.querySelector('.btn--hold');
// ----
//Starting Conditions
let scores, currentScore, activePlayer, playing;
const init = function () {
  scores = [0, 0, 0, 0];
  currentScore = 0;
  activePlayer = 0;
  playing = true;
  //--
  diceEl.classList.add('hidden');
  //--
  score0El.textContent = 0;
  score1El.textContent = 0;
  score2El.textContent = 0;
  score3El.textContent = 0;
  //--
  current0El.textContent = 0;
  current1El.textContent = 0;
  current2El.textContent = 0;
  current3El.textContent = 0;
  //--
  player0El.classList.remove(`player--winner`);
  player1El.classList.remove(`player--winner`);
  player2El.classList.remove(`player--winner`);
  player3El.classList.remove(`player--winner`);
  player0El.classList.add(`player--active`);
  player1El.classList.remove(`player--active`);
  player2El.classList.remove(`player--active`);
  player3El.classList.remove(`player--active`);
};
init();
//--

const switchPlayer = function () {
  document.getElementById(`current--${activePlayer}`).textContent = 0;
  currentScore = 0;
  // activePlayer = activePlayer === 0 ? 1 : 0;
  if (activePlayer <= 0) {
    player0El.classList.remove(`player--active`);
    player1El.classList.add(`player--active`);
    player2El.classList.remove(`player--active`);
    player3El.classList.remove(`player--active`);

    activePlayer = 1;
  } else if (activePlayer <= 1) {
    player0El.classList.remove(`player--active`);
    player1El.classList.remove(`player--active`);
    player2El.classList.add(`player--active`);
    player3El.classList.remove(`player--active`);

    activePlayer = 2;
  } else if (activePlayer <= 2) {
    player0El.classList.remove(`player--active`);
    player1El.classList.remove(`player--active`);
    player2El.classList.remove(`player--active`);
    player3El.classList.add(`player--active`);

    activePlayer = 3;
  } else if (activePlayer <= 3) {
    player0El.classList.add(`player--active`);
    player1El.classList.remove(`player--active`);
    player2El.classList.remove(`player--active`);
    player3El.classList.remove(`player--active`);

    activePlayer = 0;
  }
};

//rolling dice functionality
btnRoll.addEventListener('click', function () {
  if (playing) {
    //1. Generating a random dice roll
    const dice = Math.trunc(Math.random() * 6) + 1;
    //   console.log(dice);

    //2. display dice
    diceEl.classList.remove('hidden');
    diceEl.src = `dice-${dice}.png`;

    //3. check rolled 1: if true, switch to next player
    if (dice !== 1) {
      //add dice to current score
      currentScore += dice;
      document.getElementById(`current--${activePlayer}`).textContent =
        currentScore;
    } else {
      //switch to next player
      switchPlayer();
    }
  }
});

//Holding the Score
btnHold.addEventListener('click', function () {
  if (playing) {
    //1. Add current core to score of active player
    scores[activePlayer] += currentScore;
    // console.log(scores[activePlayer]);

    document.getElementById(`score--${activePlayer}`).textContent =
      scores[activePlayer];

    //2. check if score is already 100
    if (scores[activePlayer] >= 20) {
      //finish the game
      playing = false;
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.add('player--winner');

      document
        .querySelector(`.player--${activePlayer}`)
        .classList.remove('player--active');
      diceEl.classList.add('hidden');
    } else {
      //switch to next player
      switchPlayer();
    }
  }
});

btnNew.addEventListener('click', function () {
  //1.Restore values
  init();
});
