from django.shortcuts import render
from games.models import Game
from django.contrib.auth.decorators import login_required
# Create your views here.


@login_required
def list_games(request):
    games = Game.objects.filter(owner=request.user)
    context = {
        "list_games": games,
    }
    return render(request, "games/list.html", context)


def index(request):

    return render(request, "games/index.html")
