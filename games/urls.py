from django.urls import path
from games.views import list_games, index

urlpatterns = [
    path("", list_games, name="list_games"),
    path("index/", index, name="index"),
    ]
