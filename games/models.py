from django.db import models
from django.conf import settings
# Create your models here.


class Game(models.Model):
    name = models.CharField(max_length=50)
    console = models.CharField(max_length=20)
    picture = models.URLField(null=True)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="games",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        name = self.name
        return name
