from django.db import models
from django.conf import settings
from projects.models import Project

# Create your models here.


class Note(models.Model):
    note = models.CharField(max_length=50)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="notes",
        on_delete=models.CASCADE,
        null=True,
    )
    def __str__(self):
        return self.note



class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )
    notes = models.ManyToManyField(
        Note,
        related_name="tasks",
    )
    
    def __str__(self):
        return self.name



