from django.urls import path
from tasks.views import create_task, list_tasks, create_notes, update_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", list_tasks, name="show_my_tasks"),
    path("notes/create", create_notes, name="create_notes"),
    path("<int:pk>/edit/", update_task, name="update_task")
]
