from django.shortcuts import render, redirect, get_object_or_404
from tasks.models import Task, Note
from tasks.forms import TaskForm, NoteForm, NoteUpdateForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def list_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "list_tasks": tasks,
    }
    return render(request, "tasks/list.html", context)




@login_required
def create_notes(request):
    if request.method == "POST":
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("update_task")
    else:
        form = NoteForm()
    context = {"form": form}
    return render(request, "notes/create.html", context)


@login_required
def update_task(request, pk):
    task = get_object_or_404(Task, id=pk)
    if request.method == "POST":
        form = NoteUpdateForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = NoteUpdateForm(instance=task)

    context = {
        "post_object": task,
        "post_form": form
    }
    return render(request, "tasks/edit.html", context)